CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION unaccent;

CREATE USER postgres_lobelia WITH PASSWORD 'xxx' CREATEDB;
--CREATE USER postgres WITH PASSWORD 'xxx';
CREATE USER postgres_n2000 WITH PASSWORD 'xxx';
CREATE USER postgres_cbnsa_znieff WITH PASSWORD 'xxx';
CREATE USER postgres_cbnsa_oafs WITH PASSWORD 'xxx';
CREATE USER cartoreadonly WITH PASSWORD 'xxx';
CREATE USER backup_obv WITH PASSWORD 'xxx';
CREATE USER restore_obv WITH PASSWORD 'xxx' SUPERUSER CREATEROLE CREATEDB;
CREATE USER postgres_read_only WITH PASSWORD 'xxx';

CREATE USER postgres_cbnsa WITH CREATEROLE PASSWORD 'xxx';
CREATE USER postgres_cbnmc WITH CREATEROLE PASSWORD 'xxx';
CREATE USER postgres_cbnbp WITH CREATEROLE PASSWORD 'xxx';
CREATE USER postgres_cbnpmp WITH CREATEROLE PASSWORD 'xxx';
CREATE USER cbnsa_user WITH PASSWORD 'xxx';
CREATE USER cbnbp_user WITH PASSWORD 'xxx';
CREATE USER cbnmc_user WITH PASSWORD 'xxx';
CREATE USER cbnpmp_user WITH PASSWORD 'xxx';


CREATE DATABASE bddflore WITH OWNER postgres;

\c bddflore;
CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION unaccent;


