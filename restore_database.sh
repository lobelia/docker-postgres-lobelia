#!/bin/bash
source .env
./dump/gen_lst.sh
docker stop lobelia_integration
docker rm lobelia_integration
docker build --build-arg ORDERED_FILE=integration_bdd --build-arg POSTGRES_PASSWORD=$POSTGRES_PASSWORD -t lobelia_integration_img .
docker run --name lobelia_integration --restart unless-stopped -p $PORT:5432 -d lobelia_integration_img
