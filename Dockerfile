# syntax=docker/dockerfile:1

FROM postgis/postgis:17-3.5
WORKDIR /app

ARG POSTGRES_PASSWORD

ENV POSTGRES_USER=postgres
ENV POSTGRES_HOST_AUTH_METHOD=trust
ENV POSTGRES_INITDB_ARGS='--encoding=UTF-8'
ENV POSTGRES_PASSWORD="$POSTGRES_PASSWORD"
ENV POSTGRES_DATABASE=bddflore

ARG ORDERED_FILE

COPY dump/bddflore.dump /app/bddflore.dump
COPY dump/lst_files/"$ORDERED_FILE".lst /app/ordered.lst
COPY init/init_db.sql /docker-entrypoint-initdb.d/1_init_db.sql
COPY init/postgres_local_dump_load.sh /docker-entrypoint-initdb.d/2_postgres_local_dump_load.sh



