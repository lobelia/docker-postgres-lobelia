#!/bin/bash
# Le but de ce script est de réduire la taille des imports de dump Postgres.
# En partant d'un seul dump physique "bddflore.dump", on peut créer autant de types de dump que l'on veut.
# Pour chaque type, on crée un fichier dans "patterns_files" qui contient les patterns à exclure dans le listing des opérations.
# Le listing des opérations restant se retrouve dans le dossier "lst_files"
# Le script réalise un simple "sed d" sur chaque ligne du pattern pour supprimer les lignes correspondantes dans le fichier ".lst" .

DUMP_FOLDER="dump"
DUMP_FILE="bddflore.dump"
LST_FOLDER="lst_files"
PATTERN_FOLDER="patterns_files"
SOURCE=$(pwd)/$DUMP_FOLDER

function generate_lst_file
{
  PATTERNS_FILE=$1
  LST_FILE="$(basename $PATTERNS_FILE).lst"

docker run -t --name "lst_file" \
            --workdir /dumps/ \
            --mount type=bind,source="$SOURCE",target=/dumps/ \
            --user $(id -u):$(id -g) \
            --rm \
            postgis/postgis:17-3.5 \
  pg_restore -l $DUMP_FILE > $LST_FOLDER/$LST_FILE

  while IFS= read -r PATTERN
  do
  sed -i " /$PATTERN/d " "$LST_FOLDER/$LST_FILE" > /dev/null 2>&1

  done < "$PATTERNS_FILE"

  echo "$LST_FOLDER/$LST_FILE has been created"
}
if [[ -d "$DUMP_FOLDER" ]]
then
    cd "$DUMP_FOLDER"/
else 
    echo "Dump folder doesn't exist in current directory"
    exit
fi 
rm -rf $LST_FOLDER/*.lst


for file in $PATTERN_FOLDER/*
do
  generate_lst_file "$file"
done
