# Image Docker du Postgres Lobelia

## Présentation

Cette image permet d'avoir en local sur son poste de travail une instance de Postgres/Postgis déjà chargée avec les structures et les données de Lobelia.

## Prérequis

1. Avoir déjà installé `Docker` sur son poste de travail, ou `Docker Desktop` si vous êtes sur Windows
2. Posséder un export de la base de données Lobelia, nommé `bddflore.dump` et généré par l'outil `pg_dump`

## Installation

### 1. Clôner ce dépôt  
`git clone git@gitlab.com:lobelia/docker_custom_images.git`
### 2. Se rendre dans le dossier racine du projet :`docker-postgres-lobelia`  
`cd docker-postgres-lobelia`
### 3. Déposer dans le dossier `dump/` un export de la base Lobelia, en le nommant `bddflore.dump`

copier le .env.sample en .env et renseigner le port et le mot de passe que vous souhaitez utiliser à l'intérieur 

### Construction d'une base pour l'intégration des données 

depuis wsl 
lancer lacommande 
``
./restore_database.sh
``
Attendre que la base se restore vous pouvez vous y connecter !


### 4. *Optionnel :* Regénérer les fichiers `.lst` en exécutant le script `gen_lst.sh` dans le dossier `dump/`  
`./dump/gen_lst.sh`  
Pour en savoir plus, lire la section [Annexe sur les `ORDERED_FILES`](#génération-de-fichiers-ordered_file-supplémentaires)
### 5. Lancer la construction de l'image 
La commande `docker build` permet de constuire l'image Postgres
C'est à cet endroit qu'il faut préciser un certain nombre de paramètres :

* `-t` sert à donner un nom à l'image  
* `--build-arg ORDERED_FILE` qui définit le type d'installation désirée :  
   * `full_bdd` : base complète, l'export est chargé dans son intégralité
   * `no_vm_bdd` : base complète, sans le rafraichissement des vues matérialisées
   * `integration_bdd` : base allégée pour exécuter des scripts d'intégration de données
   * `formatage_bdd` : base très allégée, pour réaliser des opérations de formatage de fichiers
   * `ultra_light_bdd` : base minimale pour réaliser des tests 
      
* `--build-arg POSTGRES_PASSWORD`  
 
#### Exemples : 

- Je construis une image appelée `lobelia_full_img` qui possèdera une base complète :

`docker build --build-arg ORDERED_FILE=full_bdd --build-arg POSTGRES_PASSWORD=password -t lobelia_full_img .`

- Je construis une image appelée `lobelia_integration_img` qui possèdera une base allégée pour réaliser des tests d'intégration :

`docker build --build-arg ORDERED_FILE=integration_bdd --build-arg POSTGRES_PASSWORD=password -t lobelia_integration_img .`
 
- Je construis une image appelée `lobelia_formatage_img` qui possèdera une base allégée pour réaliser des tests d'intégration :

`docker build --build-arg ORDERED_FILE=formatage_bdd --build-arg POSTGRES_PASSWORD=password -t lobelia_formatage_img .`

### 6. Créer un conteneur à partir de l'image créée

Il est impératif de créer un pont entre les ports du conteneur et de l'hôte, sinon Postgres ne sera pas joignable.

#### Exemples :

- Je crée un conteneur appelé `lobelia_full` à partir de l'image `lobelia_full_img` et connecte son port `5432` sur mon port `8888` :  

`docker run --name lobelia_full --restart unless-stopped -p 8888:5432 -d lobelia_full_img`


- Je crée un conteneur appelé `lobelia_integration` à partir de l'image `lobelia_integration_img` et connecte son port `5432` sur mon port `8889` :

`docker run --name lobelia_integration --restart unless-stopped -p 8889:5432 -d lobelia_integration_img`

- Je crée un conteneur appelé `lobelia_formatage` à partir de l'image `lobelia_formatage_img` et connecte son port `5432` sur mon port `8888` :

`docker run --name lobelia_formatage --restart unless-stopped -p 8888:5432 -d lobelia_formatage_img`

**Cette étape est potentiellement la plus longue car `pg_restore` importe toute la base de données.**  
En fonction de la `ORDERED_FILE` sélectionnée, cela peut prendre de nombreuses minutes.

### 7. Connexion à la base

Une fois l'initialisation de la base terminée, le conteneur redémarre et la base est prête à l'usage.  
On peut s'y connecter directement depuis `PG ADMIN` ou `DBeaver`.  

L'hôte sera `localhost` et le port sera celui spécifié à l'étape précédente : `8889` dans notre dernier exemple.  
Le nom de la base (`bddflore`), le user (`postgres`) et le mot de passe (`POSTGRES_PASSWORD`) celui spécifié durant l'étape 5.

# Annexes

## Génération de fichiers `ORDERED_FILE` supplémentaires

Afin de générer d'autres fichiers `ORDERED_FILE` (extension `.lst`), un outil est présent dans le dossier `dump/`

### 1. Créer un fichier texte sans extension contenant sur chaque ligne un motif à supprimer et le placer dans le dossier `dump/patterns_files`  

*Exemples :*


integration_bdd
```
maille  
znieff  
natura_2000

```
**Il faut respecter la syntaxe de la fonction `sed`, en remplaçant les espaces par des `\s`**

no_vm_bdd
```
MATERIALIZED\sVIEW\sDATA

```
**Le fichier doit impérativement se terminer par une ligne vide !**

2. Regénérer tous les fichiers `.lst` en exécutant le script `gen_list.sh`

`./dump/gen_lst.sh`

3. Un fichier `lst_files/integration_bdd.lst` est créé parmi les autres fichiers `.lst`, et peut être utilisé dans l'étape 4 de l'installation